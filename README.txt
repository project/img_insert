Here's the view I'm using as an example:

  $view = new stdClass();
  $view->name = 'image_gallery_edit';
  $view->description = '';
  $view->access = array (
  0 => '1',
  1 => '2',
);
  $view->view_args_php = '$view->gridcount = 3;
';
  $view->page = TRUE;
  $view->page_title = 'Image Galleries';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'bonus_grid';
  $view->url = '';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '20';
  $view->sort = array (
  );
  $view->argument = array (
  );
  $view->field = array (
    array (
      'tablename' => 'node_data_field_image_0',
      'field' => 'field_image_0_fid',
      'label' => '',
      'handler' => 'content_views_field_handler_ungroup',
      'options' => 'image_teaser_linked',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'image',
),
    ),
    array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array (
      'tablename' => 'term_node_2',
      'field' => 'tid',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
),
    ),
  );
  $view->exposed_filter = array (
    array (
      'tablename' => 'term_node_2',
      'field' => 'tid',
      'label' => '',
      'optional' => '0',
      'is_default' => '0',
      'operator' => '1',
      'single' => '1',
    ),
  );
  $view->requires = array(node_data_field_image_0, node, term_node_2);
  $views[$view->name] = $view;

